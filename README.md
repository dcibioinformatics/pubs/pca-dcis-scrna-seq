# PCA DCIS scRNA-Seq

This repository contains the scripts used for the manuscript: **Single Cell Expression Analysis of Ductal Carcinoma in Situ Identifies Complex Genotypic-Phenotypic Relationships Altering Epithelial Composition**

- **Raw data:** The datasets generated in this study, including sample sets 1 and 2, have been deposited in the HTAN database (https://humantumoratlas.org) under the HTAN study **phs002371**, along with the publicly available dataset for Sample Set 3. Access to these datasets requires dbGaP approval, which can be requested at dbGaP study page, https://www.ncbi.nlm.nih.gov/projects/gap/cgi-bin/study.cgi?study_id=phs002371. Dataset mapping IDs are provided in Supplementary Table 1. All other raw data are available upon request from the corresponding author.

- **Repository contents**
    - `Containers/`: contains definition files for building container images with software/packages used for the analysis, along with version details documented in the README file.
        - On a standard desktop computer, the `Singularity` container build process usually takes approximately 10 min for `cellranger`, 8h for the `R` container.
        - Please note that a valid key is required for the 10x Genomics software Cellranger.
    - `Setup/`: Contains `R` scripts that define paths, aesthetics, and utility functions. 
    - `Cellranger/`: Contains the raw single-cell RNA sequencing preprocessing pipeline based on `CellRanger`, to generate fastq files, bam files and count matrices from raw bcl files. The runtime for processing all samples is approximately 4 days.
    - `scRNAseq/`: Contains scripts for the analysis of single-cell RNA sequencing data.
        - `00_Preprocessing.Rmd`: Imports cellranger outputs to R, creates Seurat objects and gene annotation.
        - `01_QC.Rmd`: Runs quality control.
        - `02_IntegratedAnalysis.Rmd`: Integrates and clusters cells across samples, and annotates cell types.
        - `03_InferCNV_NoGroup.Rmd`: Infers CNV status for epithelial cells.
        - `04_PseudoBulkDE_NoGroup.Rmd`: Creates pseudobulk samples and runs DE analysis for different contrasts.
        - `05_Extended_DE.Rmd`: Runs cell-level DE analysis between subclones for each patient.
        - `06_Clustering_Separate.Rmd`: Runs trajectory analysis.
        - `scRNASeq_BreastKumar_Normal.Rmd`: Inspects Kumar scrnaseq data and creates the reference count matrix used in Breast bulk data cell fraction imputation.
        - `scRNASeq_PMCA10F_Cellline.Rmd`: Inspects MCF10A cellline scrnaseq data and creates the UMAPs.
    - `BulkRNAseq/`: Contains CIBERSORTx analysis scripts for bulk RNA sequencing data sets.
        - `CIBERSORTx_BreastKumar_Normal.Rmd`: Imputes cell type fractions for the Breast bulk data sets.

- **License**

This project is licensed under the **GNU General Public License v3.0** - see the [LICENSE](./LICENSE) file for details.

- **Analysis workflow**
![](./workflow.png)

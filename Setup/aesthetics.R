library(tidyverse)
library(RColorBrewer)
library(ggpubr)
library(ggsci)
library(circlize)
# make sure to use `limits = force` in `scale_*_manual()` to suppress the display of unused levels in legends.

# ------- Sample types --------
colors_stypes <- c(
  "Normal" = "#1982c4", 
  "Normal/DCIS" = "#666666",
  "DCIS" = "#ffca3a",
  "DCIS_ER-" = "#FF924C",
  "DCIS_ER+" = "#ff595e",
  "Invasive" = "#f7aef8",
  # tbcrc, rabht lcm, rahbt revised
  "DCIS_only" = "#8ac926",
  "NoRecurrence" = "#ffca3a",
  "DCIS_primary" = "#ffca3a",
  "DCIS_recurrence" = "#ff595e",
  "DCISRecurrence" = "#ff595e",
  "IBCRecurrence" = "#f7aef8",
  "IBC_recurrence" = "#f7aef8",
  "INV_recurrence" = "#f7aef8",
  # TA578
  "Node" = "#b15928",
  "Early_neoplasia" = "#66c2a5"
)
levels_stypes <- c("Normal", "DCIS", "Normal/DCIS", "Invasive")

# ------- Cell types -------
colors_ctypes <- c(
  # Kumar
  "B" = "#666666",
  "B cells" = "#666666",
  "T" = "#d01c8b",
  "T cells" = "#d01c8b",
  "Lymphatic" = "#66c2a5",
  "Fibroblasts" = "#ffffb3",
  "Myeloid" = "#7b3294",
  "Vascular" = "#b15928",
  "Perivascular" = "#80b1d3",
  "Basal" = "#b3de69",
  "Basal/Myoep" = "#b3de69",
  "LumSec" = "#fdb462",
  "LumHR" = "#fb8072",

  "LumSec-basal" = "#E0AD99",
  "LumSec-HLA" = "#e31a1c",
  "LumSec-KIT" = "#5e60ce", 
  "LumSec-lac" = "#0d3c55",
  "LumSec-major" = "#ebc844",
  "LumSec-myo" = "#f16c20",
  "LumSec-prol" = "#370617", 
  "LumHR-active" = "#ffe8d6",
  "LumHR-major" = "#B8DEE6",
  "LumHR-SCGB" = "#ffd6ff", 
  
  "Unknown" = "lightgrey",
  "Other" = "lightgrey",
  "NA" = "white"
  
)

levels_ctypes_kumar_3ep <- c("Basal/Myoep", "LumSec", "LumHR")
levels_ctypes_kumar_11ep <- c("Basal/Myoep", "LumSec-basal", "LumSec-HLA", "LumSec-KIT", 
                               "LumSec-lac", "LumSec-major", "LumSec-myo", "LumSec-prol", 
                               "LumHR-active", "LumHR-major", "LumHR-SCGB")
levels_ctypes_kumar_3nonep <- c("Fibroblasts", "Perivascular", "Vascular")
levels_ctypes_kumar_7nonep <- c("B cells", "Fibroblasts", "Lymphatic", "Myeloid", 
                                "Perivascular", "T cells", "Vascular")
levels_ctypes_kumar_3ep7nonep <- c(levels_ctypes_kumar_3ep, levels_ctypes_kumar_7nonep, "Unknown")
levels_ctypes_kumar_11ep7nonep <- c(levels_ctypes_kumar_11ep, levels_ctypes_kumar_7nonep, "Unknown")

# ------- CNV groups only (merged, separated) -------
colors_cnvs <- c(
  # "Normal-Normal" = "#1982c4",
  # "DCIS-Normal" = "#8ac926",
  # "DCIS-DCIS" = "#ffca3a",
  "NN" = "#1982c4",
  "DN/NN" = "#52A675",
  "DNNN" = "#52A675",
  "DN" = "#8ac926",
  "DD" = "#ffca3a",
  "DD_ER-" = "#FF924C",
  "DD_ER+" = "#ff595e"
)
levels_cnver <- c("DNNN", "DD_ER-", "DD_ER+")

# ------- ER only -------
colors_ers <- c(
  "NA" = "lightgrey",
  "-" = "#c2a5cf",
  "+" = "#ffca3a"
)
levels_ers <- c("NA", "-", "+")

# ------- Patients/cases -------
levels_cases <- c(
  "2", "6", "10", "12", "16",
  "PCA-001", "PCA-19-005", 
  "PCA-20-025", "PCA-20-028", "PCA-20-030", "PCA-20-033", "PCA-20-035", "PCA-20-037", 
  "PCA-21-042", "PCA-21-050", "PCA-22-053"
)
# colors_cases <- ggColorHue(length(levels_cases))
colors_cases <- colorRampPalette(brewer.pal(name="Dark2", n = 8))(length(levels_cases))
names(colors_cases) <- levels_cases
colors_cases <- c(colors_cases, `PCA-20-035\nNormal/DCIS` = as.character(colors_cases["PCA-20-035"]))

levels_cases_short <- sprintf("%02d", 1:16)
colors_cases_short <- colorRampPalette(brewer.pal(name="Dark2", n = 8))(length(levels_cases_short))
names(colors_cases_short) <- levels_cases_short

# ------- Gene marker sources -------
colors_sources <- c(
  "Kumar Major" = "#ffffb3",
  "Kumar State" = "#80b1d3",
  "Kumar_6" = "#ffffb3",
  "Kumar_11" = "#80b1d3"
)

# ------- Subclone -------
levels_subclones <- paste0("s", 1:8)
colors_subclones <- c(ggpubr::get_palette(palette = "npg", 7), "#ffeda0") # scales::show_col(colors_subclones)
names(colors_subclones) <- levels_subclones

# ------- Continuous scale -------
colors_expr <- colorRamp2(seq(-4, 4, length = 5), 
                          c("#053061", "#2166ac", "white", "#b2182b", "#67001f"))


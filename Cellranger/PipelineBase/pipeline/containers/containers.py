from pathlib import Path
from os import sys, mkdir
import subprocess as sp
import copy
from enum import Enum, auto

class System(Enum):
  singularity = auto()
  docker = auto()

  def link(self, entries, perm, base='/'):
    if self is System.singularity:
      return [y for x in
              (('-B',
                f'{entry[1]}:' +
                f'{"" if base is Path(entry[0]).root else base}' +
                f'{entry[0]}:{perm}')
               for entry in entries) for y in x]

    elif self is System.docker:
      return [y for x in
              (('--mount',
                f'type=bind,source={entry[1]},' +
                f'target={"" if base is Path(entry[0]).root else base}' +
                f'{entry[0]}' +
                f',RW={perm=="rw"}')
               for entry in entries) for y in x]

    
  def check(self, cmd, container):
    if self is System.docker:
      sp.run(['docker', 'pull', container], shell=False).check_returncode()
      cmd['docker'] = container
      
    elif self is System.singularity:     
      p = Path(container)
          
      if p.exists():
        cmd['singularity'] = str(p.resolve())
      elif container[:10] == "library://":
        cmd['singularity'] = container        
      else:
        raise Exception(f'No such file: {p}')

      
  def command(self, cmd, sample, settings):
    get_links(cmd, sample, settings)

    if self is System.docker:
      if not settings.get('print', False):
        command = ['docker', 'run']     
        
        q = ('\'' if settings.get('print', False)
             or settings.get('test', 0) > 0 else '')

        command.extend(self.get_link_files(cmd))
        command.extend([cmd['docker'], 'bash', '-c', f'{q}cd / && {" ".join(cmd["cmd"])}{q}'])

        cmd['cmd_resolved'] = command

      else:
        cmd['cmd_resolved'] = cmd['cmd']

    elif self is System.singularity:
      if not settings.get('print', False) and cmd.get('singularity', False):
        command = ['singularity', 'exec', '--no-home']     
        
        q = ('\'' if settings.get('print', False)
             or settings.get('test', 0) > 0 else '')

        command.extend(self.get_link_files(cmd))
          
        if 'app' in cmd:
          command.extend(['--app', cmd['app'], cmd['singularity'], 'bash', '-c',
                          f'{q}cd / &&  {" ".join(cmd["cmd"])}{q}'])
        else:
          command.extend([cmd['singularity'], 'bash', '-c',
                          f'{q}cd / && {" ".join(cmd["cmd"])}{q}'])

        cmd['cmd_resolved'] = command

    else:
      cmd['cmd_resolved'] = cmd['cmd']


  def get_link_files(self, cmd):
    return [x for x in self.link(cmd['output_files'], 'rw') +
            self.link(cmd['input_files'], 'ro')]

      
def find_first(x, *items):
  for it in items:
    if it.get(x, None) is not None:
      return True, it.get(x, False)
  return False, False


# priority: samples, command, then setting
def get_param(key, sample, command, settings):
  found, it = find_first(key, sample, command, settings)
  
  if found:
    return it
  elif settings.get('print', False):
    if key not in settings['print']['parameters']: 
      settings['print']['parameters'].append(key)
  else:
    raise Exception(f'No parameter definition for {key}')
  

def resolve_dir(fname, paths, root):
  for x in fname.iterdir():
    if x.is_dir():
      paths = resolve_dir(x, paths, root / fname.stem)
    elif x.is_file():
      paths.append((root / x.name, x))
  return paths

  
def get_links(cmd, sample, settings):
  links = []
    
  if 'link' in cmd:
    if 'links' not in settings:
      raise Exception('No links defined in settings file')

    for link in cmd['link']:
      if link in settings.get('links', []):
        lnk = {key: val for key, val in settings['links'][link].items()}

        if 'parameters' in lnk:
          params = {key: get_param(key, sample, cmd, settings)
                    for key in lnk['parameters']}
          lnk['target'] = lnk['target'].format(**params)
          lnk['file'] = lnk['file'].format(**params)

        fname = Path(lnk['file'])

        if settings.get('print', False):
          if not fname.exists() and not lnk.get('create', False):
            settings['print']['files'].append(lnk['file'])

        elif settings.get('test', 0) > 0:
          if not fname.exists() and not lnk.get('create', False):
            raise Exception(f'Link file does not exist: {lnk["file"]}')

        else:
          if not fname.exists() and lnk.get('create', False):
            fname.mkdir()  
                             
          elif not fname.exists():
            raise Exception(f'Link file does not exist: {lnk["file"]}')

        if lnk.get('clean', False) or lnk.get('create', False):
          if 'output_files' in cmd:
            cmd['output_files'].append((lnk['target'], fname.resolve()))
          else:
            cmd['output_files'] = [(lnk['target'], fname.resolve())]
        else:
          val = 'output_files' if lnk.get('rw', False) else 'input_files'
          cmd[val] = resolve_dir(fname, cmd.get(val, []), Path(lnk['target']))


def find_container(*items):
  for it in items:
    if it.get('singularity', False):    
      return System.singularity, it.get('singularity', False)
    if it.get('docker', False):
      return System.docker, it.get('docker', False)
  return False, False


def resolve_containers(commands, sample, settings):
  for cmd in commands:
    if 'parameters' in cmd:
      env = {key: get_param(key, sample, cmd, settings) for key in cmd['parameters']}
      cmd['cmd'] = [item.format(**env) for item in cmd['cmd']]

    system, container = find_container(cmd, sample, settings)
    
    if container and ('parameters' in cmd):
      container = container.format(**env)
    
    if system:
      system.check(cmd, container)
      system.command(cmd, sample, settings)
      
    else:
      cmd['cmd_resolved'] = cmd['cmd']

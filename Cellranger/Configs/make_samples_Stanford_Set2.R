#!/usr/bin/Rscript

stage <- commandArgs(trailingOnly = T)
print(stage)

wd <- "/mnt/data1/workspace/HTAN/scRNASeq"
sapply(list.files(file.path(wd, "Code/Setup"), full.names = T), source)

sc_ver <- "BPCA"
batch_id <- "Stanford_Set2"
run_id <- batchinfo[[batch_id]][["run_id"]]
ss_file <- batchinfo[[batch_id]][["ss_file"]]

file_ss_13 <- file.path(dir_cr, "simple.csv")
file_ss_2 <- file.path(dir_cr, "simple_210202.csv")

if(stage == "stage1"){
  
  ss <- glue(bpca_ss_file) %>%
    read.csv(header = T, stringsAsFactors = F, skip = 12) %>%
    transmute(
      Lane = "*", 
      Sample = paste0(Sample_ID, "_", Sample_Name), 
      Index = index
    ) %>%
    distinct()
  ss %>% write.csv(file = glue(cr_ss_file), row.names = F, quote = F)
  ss %>% mutate(Lane = "2") %>% write.csv(file = cr_ss_file_sf2_run2, row.names = F, quote = F)
  
  data.frame(
      uid = run_id,
      bcldir = glue(bpca_bcl_dir)
    ) %>%
    mutate(samplesheet = c(glue(cr_ss_file), cr_ss_file_sf2_run2, glue(cr_ss_file))) %>%
    writeYaml(yaml_file = glue(cr_samples1_yml))
  
} else if(stage == "stage2"){
  
  ss <- glue(cr_ss_file) %>%
    read.csv(header = T, stringsAsFactors = F)
  sample_id <- ss[["Sample"]]
  
  expand.grid(run_id = run_id, sample_id = sample_id) %>%
    transmute(
      uid = sample_id,
      sample = sample_id, # input prefix
      fastq = glue(cr_mkfastq_prefix)
    ) %>%
    group_by(uid, sample) %>%
    summarize(fastqs = paste(fastq, collapse = ",")) %>%
    ungroup() %>%
    writeYaml(yaml_file = glue(cr_samples2_yml))
  
} else {
  stop("Invalid argument. Please provide either 'stage1' or 'stage2'.")
}
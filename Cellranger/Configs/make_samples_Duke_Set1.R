#!/usr/bin/Rscript

stage <- commandArgs(trailingOnly = T)
print(stage)

wd <- "/mnt/data1/workspace/HTAN/scRNASeq"
sapply(list.files(file.path(wd, "Code/Setup"), full.names = T), source)

sc_ver <- "BPCA"
batch_id <- "Duke_Set1"
run_id <- batchinfo[[batch_id]][["run_id"]]
ss_file <- batchinfo[[batch_id]][["ss_file"]]

if(stage == "stage1"){
  
  ss <- glue(bpca_ss_file) %>%
    read.csv(header = T, stringsAsFactors = F, comment.char = "#")
  write.csv(ss, file = glue(cr_ss_file), row.names = F, quote = F)
  
  data.frame(
      uid = run_id,
      bcldir = glue(bpca_bcl_dir)
    ) %>%
    mutate(samplesheet = glue(cr_ss_file)) %>%
    writeYaml(yaml_file = glue(cr_samples1_yml))
  
} else if(stage == "stage2"){
  
  ss <- glue(cr_ss_file) %>%
    read.csv(header = T, stringsAsFactors = F)
  sample_id <- ss[["Sample"]]
  
  expand.grid(run_id = run_id, sample_id = sample_id) %>%
    transmute(
      uid = sample_id,
      sample = sample_id, # input prefix
      fastq = glue(cr_mkfastq_prefix)
    ) %>%
    group_by(uid, sample) %>%
    summarize(fastqs = paste(fastq, collapse = ",")) %>%
    ungroup() %>%
    writeYaml(yaml_file = glue(cr_samples2_yml))
  
} else {
  stop("Invalid argument. Please provide either 'stage1' or 'stage2'.")
}
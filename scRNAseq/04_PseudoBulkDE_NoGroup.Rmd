---
title: "Pseudo-Bulk Differential Expression Analysis"
subtitle: "Epithelial cells based on Kumar Hybrid markers"
output: 
  html_document:
    toc: true
    toc_depth: 3
    toc_float: 
      collapsed: false
    code_folding: hide
    number_sections: true
editor_options: 
  chunk_output_type: console
---
```{css, echo=F}
/* turn level 4/5 headings into bulleted lists */
/* padding: t r b l */
h4:before {
  content: '\2022';
  padding: 0px 10px 0px 0px;
}
h4 {
  font-size: 15px;
  padding: 0px 0px 0px 10px;
}
h5:before {
  content: '\25CB';
  padding: 0px 10px 0px 0px;
}
h5 {
  font-size: 14px;
  padding: 0px 0px 0px 20px;
}
table {
  white-space: nowrap;
}
```


# Prerequisites

#### Container {-}
```{bash eval=F}
wd=/mnt/data1/workspace/HTAN/scRNASeq
wd2=/mnt/data2/studydata/HTAN/scRNASeq
sif=/mnt/data1/public/singularity/dcibioinformaticsR-v2.1.sif
singularity exec --no-home -B ${wd}:${wd}:rw -B ${wd2}:${wd2}:ro ${sif} R
```

#### Packages and paths {-}
```{r setup, echo=T, message=F, warning=F, results='hide'}
sc_ver <- "BPCA"
flt_ver <- "ByQC"
int_ver <- "Batch"
ct_ver <- "scSorter0p2_KumarHybrid"
cnv_ver <- "MixedPON_NoGroup_leiden"
gene_anno_ver <- "Cellranger2020a"
gene_set_ver <- "230814"
ds_ver <- "DS3k"

wd <- "/mnt/data1/workspace/HTAN/scRNASeq"
sapply(list.files(file.path(wd, "Code/Setup"), full.names = T), source)

obj_ver <- basename(glue(seurat_cnv_prefix))
createDirs(glue(pb_dir))

preprocess <- F
```

#### Load `Seurat` object {-}
```{r eval=preprocess}
obj_cnv <- readRDS(glue(seurat_cnv_obj))
obj <- obj_cnv
```

#### Load gene annotation {-}
```{r eval=preprocess}
gene_anno <- readRDS(glue(gene_anno_obj))
```

#### Load gene sets {-}
```{r eval=preprocess}
loadGeneSets(
  gene_anno = gene_anno, 
  convert_to = "Ensembl",
  entrez_var = "Entrez",
  ensembl_var = "Ensembl",
  hgnc_var = "HGNC_Unique",
  output_rdata_file = glue(gene_set_data)
)
```

```{r}
load(glue(gene_set_data))
```


# DE between Neoplastic Status (Fig1D, SuppFig2D, SuppTab4)

- Pseudobulks are aggregated by `Neoplastic Status` + `scRNA_sample_ID`

```{r}
pb_ver <- "CaseID+ER+CNV"
if(!preprocess){
  cfg <- readRDS(glue(pb_configs))
  de_list <- readRDS(glue(pb_de_out_list))
  fgsea_list_all <- readRDS(glue(pb_gset_out_list))
}
```

## Processing steps

#### Configurations {-}
```{r}
# contrasts
# "CNV_ER_mergedNN" "CNV_ER_mergedDN" "CNV_ER_mergedDD_ERneg" "CNV_ER_mergedDD_ERpos" "SourceStanford"
contrast_list <- list(
  `DD vs. DN/NN` = c(-1/2, -1/2, 1/2, 1/2, 0),
  `DD vs. NN` = c(-1, 0, 1/2, 1/2, 0),
  `DD vs. DN` = c(0, -1, 1/2, 1/2, 0),
  `DN vs. NN` = c(-1, 1, 0, 0, 0),
  `DD_ER- vs. DN/NN` = c(-1/2, -1/2, 1, 0, 0),
  `DD_ER+ vs. DN/NN` = c(-1/2, -1/2, 0, 1, 0),
  `DD_ER+ vs. DD_ER-` = c(0, 0, -1, 1, 0)
)

cfg <- list(
  id_vars = c("scRNA_case_ID", "ER", "CNV_group_short"),
  filters = c("!is.na(CNV_group_short)"),
  
  formula = "~ 0 + CNV_ER + Source",
  contrast_list = contrast_list,
  gene_set_collections = c("KEGG", "MSigDB_H", "MSigDB_C1"),

  split_var = "CNV_group_short",
  anno_list = list(
    CNV_group_short = list(colors = colors_cnvs, name = "Neoplastic Status"),
    ER = list(colors = colors_ers, name = "ER Status"),
    scRNA_case_ID_short = list(colors = colors_cases_short, name = "Patient"),
    Log_Cell_Count = list(name = "log(Cell Count)")
  )
)
saveRDS(cfg, file = glue(pb_configs))
```

#### Create Pseudobulk `DESeq2` object {-}
```{r eval=preprocess}
dds <- createPseudoBulkDDS(
  object = obj, 
  gene_anno = gene_anno, 
  id_vars = cfg$id_vars
)
dds <- dds[rowSums(counts(dds)) >= 10, ]
vsd <- vst(dds)
# pryr::object_size(dds)
saveRDS(dds, file = glue(pb_de_in_dds))
saveRDS(vsd, file = glue(pb_de_in_vsd))
# dds <- readRDS(glue(pb_de_in_dds))
# vsd <- readRDS(glue(pb_de_in_vsd))
```

#### Run `DESeq2` {-}
```{r eval=preprocess}
de_list <- runDESeq2(
  object = dds,
  formula_str = cfg$formula,
  contrast_list = cfg$contrast_list,
  filters = cfg$filters,
  inter_file = glue(pb_de_out_dds)
)
saveRDS(de_list, file = glue(pb_de_out_list))

de_sig_list <- de_list %>%
  purrr::map(~ filter(.x, padj < cutoff_qval)) %>%
  Filter(Negate(isNullDF), .) # str(de_sig_list, 1)
names(de_sig_list) <- gsub("/", "", names(de_sig_list))
write.xlsx(de_sig_list, file = glue(pb_de_out_xlsx))
```

#### Run `fgsea` {-}
```{r eval=preprocess}
# set up seeds and run fgsea
fgsea_list_all <- list()

set.seed(1723)
seeds <- sample(1:10000, length(cfg$gene_set_collections), replace = F)
names(seeds) <- cfg$gene_set_collections

for(gs_ver in cfg$gene_set_collections){
  fgsea_list_all[[gs_ver]] <- runFgsea(
    pathways = gsets[[gs_ver]],
    de_list = de_list,
    stat_var = "stat",
    gene_var = "Ensembl",
    seed = seeds[gs_ver],
    max_gset_size = 500,
    min_gset_size = 15,
    bp_workers = 3,
    gene_anno = gene_anno,
    from_var = "Ensembl",
    to_var = "HGNC"
  )
}
# str(fgsea_list_all, 2)
saveRDS(fgsea_list_all, file = glue(pb_gset_out_list))
```


## Differentially expressed genes {.tabset}
```{r results='asis', fig.width=14, fig.height=7}
n_tops <- 30

for(de_ver in names(de_list)){
  cat("### ", de_ver, " {.tabset -} \n\n")
  cat("#### Table of top", n_tops, "DEGs {-} \n\n")
  de_list[[de_ver]] %>%
    filter(padj < cutoff_qval) %>%
    head(n = n_tops) %>%
    printKable()
  cat("\n\n")
}
```

## Pseudobulk heatmaps {.tabset}
```{r}
DDvsNN_Top500DEGs <- de_list[["DD vs. NN"]] %>%
  filter(padj < 0.05) %>%
  mutate(s = sign(log2FoldChange)) %>%
  group_by(s) %>%
  slice_min(order_by = pvalue, n = 250) %>%
  pull(Ensembl)

gsets_hm <- list(
  DDvsNN_Top500DEGs = list(
    gset_name = NULL, #"Top 500 DEGs (250 Up & 250 Down) in DD vs. NN",
    gene_names = DDvsNN_Top500DEGs
  )
)

for(hm_ver in names(gsets_hm)){
  cat("###", hm_ver, "{-} \n\n")
  img <- glue(pb_de_heatmap)
  cat("![](", img, ") \n\n")
}
```

```{r eval=preprocess}
for(hm_ver in names(gsets_hm)){
  print(hm_ver)
  
  gene_names <- gsets_hm[[hm_ver]][["gene_names"]]
  gset_name <- gsets_hm[[hm_ver]][["gset_name"]]
  
  vsd0 <- filterObject(object = vsd, 
                       col_filters = c(cfg$filters, "Cell_Count > 10"))
  
  rids <- apply(assay(vsd0), 1, stats::var) != 0 & rownames(vsd0) %in% gene_names
  vsd1 <- vsd0[rids, ]
  
  cdat <- colData(vsd1) %>%
    as.data.frame() %>%
    mutate(across(where(is.factor), droplevels))
  mat <- scaleRows(assay(vsd1))
  rownames(mat) <- convertGeneIDs(rownames(mat), gene_anno, "Ensembl", "HGNC")

  # cell_height <- ifelse(nrow(mat) > 50, 0.2, 1)
  # row_fs <- ifelse(nrow(mat) > 50, 11, 13)

  hm <- plotHeatmap(
    matrix = mat,
    fill_title = "Expression",
    fill_values = colors_expr,
    column_data = cdat,
    column_anno_list = cfg$anno_list,
    column_split_var = cfg$split_var,
    show_column_names = F,
    show_row_names = (length(gene_names) <= 250),
    row_title = gset_name,
    color_seed = 1723,
    legend_pos = "right",
    heatmap_width = unit(3.5, "in"),
    heatmap_height = unit(6.5, "in"),
    img_file = glue(pb_de_heatmap),
    output_pdf = T
  )
}
```

## Significant pathways
```{r results='asis'}
for(gs_ver in cfg$gene_set_collections){ # gs_ver <- "MSigDB_H"
  
  cat("###", gs_ver, " {.tabset} \n\n")
  
  pathway_levels <- fgsea_list_all[[gs_ver]][["DD vs. DN/NN"]] %>%
    arrange(NES) %>%
    pull(pathway)
  contrast_levels <- names(fgsea_list_all[[gs_ver]])
  
  fgsea_list <- fgsea_list_all[[gs_ver]]
  tmp_tab <- fgsea_list %>%
    Filter(Negate(isNullDF), .) %>%
    map_dfr(~ .x, .id = "tmp")
  
  # ----------- full ------------
  fgsea_tab <- tmp_tab %>%
    mutate(
      Contrast = factor(tmp, levels = contrast_levels),
      Pathway = factor(pathway, 
                       levels = pathway_levels, 
                       labels = gsub("HALLMARK_", "", pathway_levels))
    ) %>%
    filter(!is.na(padj)) %>%
    filter(padj < cutoff_qval) 

  anno_data <- fgsea_tab %>%
    dplyr::select(Contrast, deg_count_text) %>%
    distinct()
  
  p <- plotGG(
    data = fgsea_tab,
    template = "Dot",
    plot_aes_list = list(x = "Contrast", y = "Pathway", 
                         color = "NES", size = "minuslog10padj"),
    x_axis_pos = "bottom",
    x_title = NULL,
    y_title = NULL,
    size_title = expression("-log10(FDR)"),
    color_scheme = "two-sided",
    anno_data = anno_data,
    anno_fun = "geom_text",
    anno_aes_list = list(x = "Contrast", y = 46, 
                         label = "deg_count_text", vjust = -2),
    strip_text_x = element_text(vjust = 5),
    text_size = 6,
    panel_spacing_x = unit(0.3, "lines"),
    plot_margin = margin(10, 0, 0, 0, "pt"),
    legend_margin = margin(0, 0, 0, 0, "pt"),
    panel_height_units = unit(6, "in"),
    panel_width_units = unit(4, "in")
  ); s <- getGGSizes(p)
  img <- glue(pb_gset_dot, dot_ver = glue("{gs_ver}_Full"))
  cat("#### Dotplot (full ver.) {-} \n\n ![](", img, ")\n\n")
  ggsave(plot = p, height = s$h, width = s$w, units = "in", dpi = 300, filename = img)
  ggsave(plot = p, height = s$h, width = s$w, units = "in", filename = gsub("png", "pdf", img))
  
  for(de_ver in names(fgsea_list_all[[gs_ver]])){
    cat("#### ", de_ver, " {-} \n\n")
    fgsea_list_all[[gs_ver]][[de_ver]] %>%
      filter(padj < cutoff_qval) %>%
      head(n = 50) %>%
      printKable()
    cat("\n\n")
  }
}
```


# DE between Neoplastic Status for each Kumar3 cell state (Fig3D, SuppFig4A, SuppTab5)

- Pseudobulks are aggregated by `Kumar 3 Cell States` + `Neoplastic Status` + `scRNA_case_ID`

```{r}
pb_ver <- "CaseID+BroadCellType+CNV"
if(!preprocess){
  cfg <- readRDS(glue(pb_configs))
  de_list <- readRDS(glue(pb_de_out_list))
  fgsea_list_all <- readRDS(glue(pb_gset_out_list))
}
```

## Processing steps

#### Configurations {-}
```{r eval=preprocess}
# "CNV_ERNN" "CNV_ERDN" "CNV_ERDD_ERneg" "CNV_ERDD_ERpos" "SourceStanford"
contrast_list <- list(
  `DD vs. DN/NN` = c(-1/2, -1/2, 1/2, 1/2, 0),
  `DD_ER- vs. DN/NN` = c(-1/2, -1/2, 1, 0, 0),
  `DD_ER+ vs. DN/NN` = c(-1/2, -1/2, 0, 1, 0)
)

cfg <- list(
  cell_types = levels_ctypes_kumar_3ep,
  id_vars = c("scRNA_case_ID_short", "ER", "CNV_group_short", "Broad_Cell_Type"),
  filters = c("!is.na(CNV_group_short) & Broad_Cell_Type %in% '{ct}'"),

  formula = "~ 0 + CNV_ER + Source",
  contrast_list = contrast_list,
  gene_set_collections = c("MSigDB_H"),

  # split_var = "CNV_group_short"
  anno_list = list(
    Broad_Cell_Type = list(colors = colors_ctypes, name = "Cell State"),
    CNV_group_short = list(colors = colors_cnvs, name = "Neoplastic Status"),
    ER = list(colors = colors_ers, name = "ER Status"), # for cell-level heatmap
    ER_filled = list(colors = colors_ers, name = "ER Status"), # for pseudobulk-level heatmap
    scRNA_case_ID_short = list(colors = colors_cases_short, name = "Patient"),
    Log_Cell_Count = list(name = "log(Cell Count)")
  )
)
saveRDS(cfg, file = glue(pb_configs))
```

#### Create Pseudobulk `DESeq2` object {-}
```{r eval=preprocess}
dds <- createPseudoBulkDDS(
  object = obj,
  gene_anno = gene_anno,
  id_vars = cfg$id_vars
)
dds <- dds[rowSums(counts(dds)) >= 10, ]
vsd <- vst(dds)
# pryr::object_size(dds)
# table(dds$Broad_Cell_Type, dds$CNV_ER)
saveRDS(dds, file = glue(pb_de_in_dds))
saveRDS(vsd, file = glue(pb_de_in_vsd))
# dds <- readRDS(glue(pb_de_in_dds))
# vsd <- readRDS(glue(pb_de_in_vsd))
```

#### Run `DESeq2` {-}
```{r eval=preprocess}
de_list <- list()
for(ct in cfg$cell_types){
  de_sub_list <- runDESeq2(
    object = dds,
    formula_str = cfg$formula,
    contrast_list = cfg$contrast_list,
    filters = glue(cfg$filters),
    inter_file = glue(pb_de_out_dds, pb_ver = glue("{pb_ver}_{ct}"))
  )
  names(de_sub_list) <- paste0(ct, ",", names(de_sub_list))
  de_list <- c(de_list, de_sub_list)
} # str(de_list, 1)

saveRDS(de_list, file = glue(pb_de_out_list))

# write a table for sig degs
de_sig_list <- de_list[!grepl("DD vs. DN/NN", names(de_list))] %>%
  purrr::map(~ filter(.x, padj < cutoff_qval)) %>%
  Filter(Negate(isNullDF), .) # str(de_sig_list, 1)
names(de_sig_list) <- gsub("/", "", names(de_sig_list))
write.xlsx(de_sig_list, file = glue(pb_de_out_xlsx))
```

#### Run `fgsea` {-}
```{r eval=preprocess}
# set up seeds and run fgsea
fgsea_list_all <- list()

set.seed(1723)
seeds <- sample(1:10000, length(cfg$gene_set_collections), replace = F)
names(seeds) <- cfg$gene_set_collections

for(gs_ver in cfg$gene_set_collections){
  fgsea_list_all[[gs_ver]] <- runFgsea(
    pathways = gsets[[gs_ver]],
    de_list = de_list,
    stat_var = "stat",
    gene_var = "Ensembl",
    seed = seeds[gs_ver],
    max_gset_size = 500,
    min_gset_size = 15,
    bp_workers = 3,
    gene_anno = gene_anno,
    from_var = "Ensembl",
    to_var = "HGNC"
  )
}
# str(fgsea_list_all, 2)
saveRDS(fgsea_list_all, file = glue(pb_gset_out_list))
```

## Significant pathways
```{r results='asis'}
for(gs_ver in cfg$gene_set_collections){ # gs_ver <- "MSigDB_H"
  
  cat("###", gs_ver, " {.tabset} \n\n")
  
  pathway_levels <- sort(names(gsets[[gs_ver]]))
  contrast_levels <- names(cfg$contrast_list) %>% setdiff("DD vs. DN/NN")
  
  fgsea_list <- fgsea_list_all[[gs_ver]]
  tmp_tab <- fgsea_list %>%
    Filter(Negate(isNullDF), .) %>%
    map_dfr(~ .x, .id = "tmp") %>%
    separate(col = "tmp", into = c("CellType", "Contrast"), sep = ",", remove = F) %>%
    filter(Contrast %in% contrast_levels)
  
  # ----------- full ------------
  fgsea_tab <- tmp_tab %>%
    mutate(
      CellType = factor(CellType, levels = cfg$cell_types),
      Contrast = gsub("DD_(ER[\\+-]) vs. DN/NN", "\\1", Contrast) %>% factor(levels = c("ER-", "ER+")),
      Pathway = factor(pathway, 
                       levels = pathway_levels, 
                       labels = gsub("HALLMARK_", "", pathway_levels))
    ) %>%
    filter(!is.na(padj)) %>%
    filter(padj < cutoff_qval) 
  
  anno_data <- fgsea_tab %>% # to keep #DEGs for panels without sig pathways
    dplyr::select(CellType, Contrast, deg_count) %>%
    distinct()
  
  p <- plotGG(
    data = fgsea_tab,
    template = "Dot",
    plot_aes_list = list(x = "Contrast", y = "Pathway", 
                         color = "NES", size = "minuslog10padj"),
    x_axis_pos = "bottom",
    x_title = NULL,
    y_title = NULL,
    size_title = expression("-log10(FDR)"),
    color_scheme = "two-sided",
    facet_formula = "~ CellType",
    facet_scales = "fixed", 
    facet_drop = F,
    anno_data = anno_data,
    anno_fun = "geom_text",
    anno_aes_list = list(x = "Contrast", y = length(pathway_levels), 
                         label = "deg_count", vjust = -0.5),
    strip_text_x = element_text(vjust = 5),
    text_size = 6,
    panel_spacing_x = unit(0.3, "lines"),
    plot_margin = margin(5, 0, 0, 0, "pt"),
    legend_margin = margin(0, 0, 0, 0, "pt"),
    panel_height_units = unit(5, "in"),
    panel_width_units = unit(0.5, "in")
  ); s <- getGGSizes(p)
  img <- glue(pb_gset_dot, dot_ver = glue("{gs_ver}_Full"))
  cat("#### Dotplot (full ver.) {-} \n\n ![](", img, ")\n\n")
  ggsave(plot = p, height = s$h, width = s$w, units = "in", dpi = 300, filename = img)
  ggsave(plot = p, height = s$h, width = s$w, units = "in", filename = gsub("png", "pdf", img))
  
  # ----------- signif ------------
  sig_pathways <- c("TNFA_SIGNALING_VIA_NFKB", "OXIDATIVE_PHOSPHORYLATION",
                    "MYC_TARGETS_V1", "INTERFERON_GAMMA_RESPONSE", 
                    "G2M_CHECKPOINT", "EPITHELIAL_MESENCHYMAL_TRANSITION", "E2F_TARGETS", 
                    "ESTROGEN_RESPONSE_EARLY", # "INFLAMMATORY_RESPONSE"
                    "ESTROGEN_RESPONSE_LATE", "MYOGENESIS")
  sig_fgsea_tab <- fgsea_tab %>%
    filter(Pathway %in% sig_pathways)
  
  p <- plotGG(
    data = sig_fgsea_tab,
    template = "Dot",
    plot_aes_list = list(x = "Contrast", y = "Pathway", 
                         color = "NES", size = "minuslog10padj"),
    x_axis_pos = "bottom",
    x_title = NULL,
    y_title = NULL,
    size_title = expression("-log10(FDR)"),
    color_scheme = "two-sided",
    facet_formula = "~ CellType",
    facet_scales = "fixed", 
    facet_drop = F,
    legend_box = "horizontal", # between legends
    legend_pos = "bottom",
    legend_dir = "horizontal", # within a legend
    legend_just = c(1, 0.5),
    text_size = 6,
    panel_spacing_x = unit(0.3, "lines"),
    plot_margin = margin(5, 1, 0, 0, "pt"),
    legend_margin = margin(0, 0, 0, 0, "pt"),
    panel_height_units = unit(1.5, "in"),
    panel_width_units = unit(0.4, "in")
  ); s <- getGGSizes(p)
  img <- glue(pb_gset_dot, dot_ver = glue("{gs_ver}_Tops"))
  cat("#### Dotplot (Top 10) {-} \n\n ![](", img, ")\n\n")
  ggsave(plot = p, height = s$h, width = s$w, units = "in", dpi = 300, filename = img)
  ggsave(plot = p, height = s$h, width = s$w, units = "in", filename = gsub("png", "pdf", img))
  
  for(de_ver in names(fgsea_list_all[[gs_ver]])){
    cat("#### ", de_ver, " {-} \n\n")
    fgsea_list_all[[gs_ver]][[de_ver]] %>%
      filter(padj < cutoff_qval) %>%
      head(n = 50) %>%
      printKable()
    cat("\n\n")
  }
}
```


# DE between Neoplastic Status for each Kumar11 cell substate (Fig3H, SuppFig4B, SuppTab6)

- Pseudobulks are aggregated by `Kumar 11 Cell Substates` + `Neoplastic Status` + `scRNA_case_ID`

```{r}
pb_ver <- "CaseID+CellType+CNV"
if(!preprocess){
  cfg <- readRDS(glue(pb_configs))
  de_list <- readRDS(glue(pb_de_out_list))
  fgsea_list_all <- readRDS(glue(pb_gset_out_list))
}
```

## Processing steps

#### Configurations {-}
```{r eval=preprocess}
# "CNV_ERNN" "CNV_ERDN" "CNV_ERDD_ERneg" "CNV_ERDD_ERpos" "SourceStanford"
contrast_list <- list(
  `DD vs. DN/NN` = c(-1/2, -1/2, 1/2, 1/2, 0),
  `DD_ER- vs. DN/NN` = c(-1/2, -1/2, 1, 0, 0),
  `DD_ER+ vs. DN/NN` = c(-1/2, -1/2, 0, 1, 0)
)
# "CNV_ERNN" "CNV_ERDN" "CNV_ERDD_ERpos" "SourceStanford"
alt_contrast_list <- list(
  `DD_ER+ vs. DN/NN` = c(-1/2, -1/2, 1, 0)
)

cfg <- list(
  cell_types = levels_ctypes_kumar_11ep,
  id_vars = c("scRNA_case_ID_short", "ER", "CNV_group_short", "Cell_Type"),
  filters = c("!is.na(CNV_group_short) & Cell_Type %in% '{ct}'"),

  formula = "~ 0 + CNV_ER + Source",
  contrast_list = contrast_list,
  gene_set_collections = c("MSigDB_H"),

  # split_var = "CNV_group_short",
  anno_list = list(
    Cell_Type = list(colors = colors_ctypes, name = "Cell State"),
    CNV_group_short = list(colors = colors_cnvs, name = "Neoplastic Status"),
    ER = list(colors = colors_ers, name = "ER Status"), # for cell-level heatmap
    ER_filled = list(colors = colors_ers, name = "ER Status"), # for pseudobulk-level heatmap
    scRNA_case_ID_short = list(colors = colors_cases_short, name = "Patient"),
    Log_Cell_Count = list(name = "log(Cell Count)")
  )
)
saveRDS(cfg, file = glue(pb_configs))
```

#### Create Pseudobulk `DESeq2` object {-}
```{r eval=preprocess}
dds <- createPseudoBulkDDS(
  object = obj,
  gene_anno = gene_anno,
  id_vars = cfg$id_vars
)
dds <- dds[rowSums(counts(dds)) >= 10, ]
vsd <- vst(dds)
# pryr::object_size(dds)
saveRDS(dds, file = glue(pb_de_in_dds))
saveRDS(vsd, file = glue(pb_de_in_vsd))
# dds <- readRDS(glue(pb_de_in_dds))
# vsd <- readRDS(glue(pb_de_in_vsd))
```

#### Run `DESeq2` {-}
```{r eval=preprocess}
# table(dds$Cell_Type, dds$CNV_ER)
alt_cts <- c("LumSec-HLA", "LumSec-myo", "LumHR-active") # no cells for "CNV_ERDD_ERneg"
dds_sub <- dds[, !(dds$CNV_ER %in% "DD_ER-" & dds$Cell_Type %in% alt_cts )]
# table(dds_sub$Cell_Type, dds_sub$CNV_ER)

de_list <- list()
for(ct in setdiff(cfg$cell_types, alt_cts)){
  de_sub_list <- runDESeq2(
    object = dds_sub,
    formula_str = cfg$formula,
    contrast_list = cfg$contrast_list,
    filters = glue(cfg$filters),
    inter_file = glue(pb_de_out_dds, pb_ver = glue("{pb_ver}_{ct}"))
  )
  names(de_sub_list) <- paste0(ct, ",", names(de_sub_list))
  de_list <- c(de_list, de_sub_list)
} 

for(ct in alt_cts){
  de_sub_list <- runDESeq2(
    object = dds_sub,
    formula_str = cfg$formula,
    contrast_list = alt_contrast_list,
    filters = glue(cfg$filters),
    inter_file = glue(pb_de_out_dds, pb_ver = glue("{pb_ver}_{ct}"))
  )
  names(de_sub_list) <- paste0(ct, ",", names(de_sub_list))
  de_list <- c(de_list, de_sub_list)
} # str(de_list, 1)

saveRDS(de_list, file = glue(pb_de_out_list))


# write a table for sig degs
de_sig_list <- de_list[!grepl("DD vs. DN/NN", names(de_list))] %>%
  lapply(function(df){
    df %>%
      filter(padj < cutoff_qval)
  }) %>%
  Filter(Negate(isNullDF), .) # str(de_sig_list, 1)
names(de_sig_list) <- gsub("/", "", names(de_sig_list))
write.xlsx(de_sig_list, file = glue(pb_de_out_xlsx))
```

#### Run `fgsea` {-}
```{r eval=preprocess}
# set up seeds and run fgsea
fgsea_list_all <- list()

set.seed(1723)
seeds <- sample(1:10000, length(cfg$gene_set_collections), replace = F)
names(seeds) <- cfg$gene_set_collections

for(gs_ver in cfg$gene_set_collections){
  fgsea_list_all[[gs_ver]] <- runFgsea(
    pathways = gsets[[gs_ver]],
    de_list = de_list,
    stat_var = "stat",
    gene_var = "Ensembl",
    seed = seeds[gs_ver],
    max_gset_size = 500,
    min_gset_size = 15,
    bp_workers = 3,
    gene_anno = gene_anno,
    from_var = "Ensembl",
    to_var = "HGNC"
  )
}
# str(fgsea_list_all, 2)
saveRDS(fgsea_list_all, file = glue(pb_gset_out_list))
```

## Significant pathways
```{r results='asis'}
for(gs_ver in cfg$gene_set_collections){ # gs_ver <- "MSigDB_H"
  
  cat("###", gs_ver, " {.tabset} \n\n")
  
  pathway_levels <- sort(names(gsets[[gs_ver]]))
  contrast_levels <- names(cfg$contrast_list) %>% setdiff("DD vs. DN/NN")
  
  fgsea_list <- fgsea_list_all[[gs_ver]]
  tmp_tab <- fgsea_list %>%
    Filter(Negate(isNullDF), .) %>%
    map_dfr(~ .x, .id = "tmp") %>%
    separate(col = "tmp", into = c("CellType", "Contrast"), sep = ",", remove = F) %>%
    filter(Contrast %in% contrast_levels)
  
  # ----------- full ------------
  fgsea_tab <- tmp_tab %>%
    mutate(
      CellType = factor(CellType, levels = cfg$cell_types),
      Contrast = gsub("DD_(ER[\\+-]) vs. DN/NN", "\\1", Contrast) %>% factor(levels = c("ER-", "ER+")),
      Pathway = factor(pathway, 
                       levels = pathway_levels, 
                       labels = gsub("HALLMARK_", "", pathway_levels))
    ) %>%
    filter(!is.na(padj)) %>%
    filter(padj < cutoff_qval) 
  
  anno_data <- fgsea_tab %>% # to keep #DEGs for panels without sig pathways
    dplyr::select(CellType, Contrast, deg_count) %>%
    distinct()
      
  p <- plotGG(
    data = fgsea_tab,
    template = "Dot",
    plot_aes_list = list(x = "Contrast", y = "Pathway", 
                         color = "NES", size = "minuslog10padj"),
    x_axis_pos = "bottom",
    x_title = NULL,
    y_title = NULL,
    size_title = expression("-log10(FDR)"),
    color_scheme = "two-sided",
    facet_formula = "~ CellType",
    facet_labeller = labeller(CellType = function(x){
      ifelse(grepl("-", x), gsub("-", "\n", x), paste0(x, "\n"))
    }),
    facet_scales = "fixed", 
    facet_drop = F,
    anno_data = anno_data,
    anno_fun = "geom_text",
    anno_aes_list = list(x = "Contrast", y = length(pathway_levels) + 1, 
                         label = "deg_count", vjust = -0.5),
    strip_text_x = element_text(vjust = 5),
    text_size = 6,
    panel_spacing_x = unit(0.3, "lines"),
    plot_margin = margin(5, 0, 0, 0, "pt"),
    legend_margin = margin(0, 0, 0, 0, "pt"),
    panel_height_units = unit(5, "in"),
    panel_width_units = unit(0.5, "in")
  ); s <- getGGSizes(p)
  img <- glue(pb_gset_dot, dot_ver = glue("{gs_ver}_Full"))
  cat("#### Dotplot (full ver.) {-} \n\n ![](", img, ")\n\n")
  ggsave(plot = p, height = s$h, width = s$w, units = "in", dpi = 300, filename = img)
  ggsave(plot = p, height = s$h, width = s$w, units = "in", filename = gsub("png", "pdf", img))
  
  # ----------- signif ------------
  sig_pathways <- fgsea_tab %>%
    group_by(Pathway) %>%
    summarize(avg = mean(minuslog10padj)) %>%
    top_n(n = 10, wt = avg) %>%
    pull(Pathway)
  sig_fgsea_tab <- fgsea_tab %>%
    filter(Pathway %in% sig_pathways)
  
  p <- plotGG(
    data = sig_fgsea_tab,
    template = "Dot",
    plot_aes_list = list(x = "Contrast", y = "Pathway", 
                         color = "NES", size = "minuslog10padj"),
    x_axis_pos = "bottom",
    x_title = NULL,
    y_title = NULL,
    size_title = expression("-log10(FDR)"),
    color_scheme = "two-sided",
    facet_formula = "~ CellType",
    facet_labeller = labeller(CellType = function(x){
      ifelse(grepl("-", x), gsub("-", "\n", x), paste0(x, "\n"))
    }),
    facet_scales = "fixed", 
    facet_drop = F,
    legend_box = "horizontal", # between legends
    legend_pos = "bottom",
    legend_dir = "horizontal", # within a legend
    legend_just = c(1, 0.5),
    text_size = 5,
    panel_spacing_x = unit(0.3, "lines"),
    plot_margin = margin(5, 1, 0, 0, "pt"),
    legend_margin = margin(0, 0, 0, 0, "pt"),
    panel_height_units = unit(1.5, "in"),
    panel_width_units = unit(0.4, "in")
  ); s <- getGGSizes(p)
  img <- glue(pb_gset_dot, dot_ver = glue("{gs_ver}_Tops"))
  cat("#### Dotplot (Top 10) {-} \n\n ![](", img, ")\n\n")
  ggsave(plot = p, height = s$h, width = s$w, units = "in", dpi = 300, filename = img)
  ggsave(plot = p, height = s$h, width = s$w, units = "in", filename = gsub("png", "pdf", img))
  
  for(de_ver in names(fgsea_list_all[[gs_ver]])){
    cat("#### ", de_ver, " {-} \n\n")
    fgsea_list_all[[gs_ver]][[de_ver]] %>%
      filter(padj < cutoff_qval) %>%
      head(n = 50) %>%
      printKable()
    cat("\n\n")
  }
}
```


# DE between Kumar3 cell states (Fig5A)

- Pseudobulks are aggregated by `Kumar 3 Cell States` + `scRNA_case_ID`

```{r}
pb_ver <- "CaseID+BroadCellType"
if(!preprocess){
  de_list <- readRDS(glue(pb_de_out_list))
}
```

## Processing steps

#### Configurations {-}
```{r eval=preprocess}
hm_ctypes <- c(levels_ctypes_kumar_3ep, "Fibroblasts")
# contrasts
contrast_mat <- matrix(-1/3, nrow = 4, ncol = 4)
diag(contrast_mat) <- 1
contrast_mat <- rbind(contrast_mat, 0) # Batch
contrast_list <- as.list(as.data.frame(contrast_mat))
names(contrast_list) <- c(hm_ctypes)
contrast_list[["Basal/Myoep_vs_Fibroblasts"]] <- c(1,0,0,-1,0)

cfg <- list(
  id_vars = c("scRNA_case_ID_short", "Broad_Cell_Type"),

  label = "EachVsRest",
  formula = "~ 0 + Broad_Cell_Type + Source",
  contrast_list = contrast_list,

  filters = c("Broad_Cell_Type %in% hm_ctypes & Cell_Count > 10"),
  split_var = "Broad_Cell_Type",
  anno_list = list(
    # columns:
    Broad_Cell_Type = list(colors = colors_ctypes, name = "Cell State"),
    CNV_group_short = list(colors = colors_cnvs, name = "Neoplastic Status"),
    ER = list(colors = colors_ers, name = "ER Status"), # for cell-level heatmap
    ER_filled = list(colors = colors_ers, name = "ER Status"), # for pseudobulk-level heatmap
    scRNA_case_ID_short = list(colors = colors_cases_short, name = "Patient"),
    Log_Cell_Count = list(name = "log(Cell Count)"),
    # rows:
    celltype = list(colors = colors_ctypes, name = "Cell State", show_legend = F),
    neglog10padj = list(name = "-log10(FDR)")
  )
)
saveRDS(cfg, file = glue(pb_configs))
```

#### Create Pseudobulk `DESeq2` object {-}
```{r eval=preprocess}
dds <- createPseudoBulkDDS(
  object = obj,
  gene_anno = gene_anno,
  id_vars = cfg$id_vars
)
dds <- dds[rowSums(counts(dds)) >= 10, ]
vsd <- vst(dds)
# pryr::object_size(dds)
saveRDS(dds, file = glue(pb_de_in_dds))
saveRDS(vsd, file = glue(pb_de_in_vsd))
# dds <- readRDS(glue(pb_de_in_dds))
# vsd <- readRDS(glue(pb_de_in_vsd))
```

#### Run `DESeq2` {-}
```{r eval=preprocess}
de_list <- runDESeq2(
  object = dds,
  formula_str = cfg$formula,
  contrast_list = cfg$contrast_list,
  filters = cfg$filters,
  inter_file = glue(pb_de_out_dds)
)
saveRDS(de_list, file = glue(pb_de_out_list))
```

## Differentially expressed genes {.tabset}
```{r results='asis', fig.width=14, fig.height=7}
n_tops <- 100

for(de_ver in names(de_list)){
  cat("### ", de_ver, " {.tabset -} \n\n")
  cat("#### Table of top", n_tops, "DEGs {-} \n\n")
  de_list[[de_ver]] %>%
    filter(padj < cutoff_qval) %>%
    head(n = n_tops) %>%
    printKable()
  cat("\n\n")
}
```

## Pseudobulk heatmaps {.tabset}
```{r results='asis'}
for(hm_ver in c("TopBM")){
  cat("###", hm_ver, "{-} \n\n")
  img <- glue(pb_de_heatmap)
  cat("![](", img, ") \n\n")
  tab <- readRDS(glue(pb_de_heatmap_rowtab))
  printKable(tab)
}
```

```{r eval=preprocess}
TopBM <- de_list[hm_ctypes] %>%
  lapply(function(l){
    l %>%
      arrange(pvalue) %>%
      filter(padj < 0.01) %>%
      filter(log2FoldChange > 0) %>%
      filter(Ensembl %in% gsets$GO[["GO:0005604 basement membrane"]]) %>%
      dplyr::select(Ensembl, HGNC, log2FoldChange, stat, pvalue, padj)
  }) %>%
  map_dfr(~ as.data.frame(.x), .id = "celltype") %>%
  mutate(rank = rank(padj),
         neglog10padj = -log10(padj)) %>%
  # arrange(pvalue)
  group_by(Ensembl) %>%
  slice_min(order_by = rank, n = 1) %>%
  ungroup() %>%
  arrange(celltype, rank)
saveRDS(TopBM, file = glue(pb_de_heatmap_rowtab, hm_ver = "TopBM"))

gsets_hm <- list(
  TopBM = list(
    gset_name = NULL, #"Up-Regulated Basement-Membrane DEGs",
    gene_names = TopBM$Ensembl,
    row_anno_tab = TopBM
  )
)

for(hm_ver in names(gsets_hm)){
  print(hm_ver)

  gene_names <- gsets_hm[[hm_ver]][["gene_names"]]
  gset_name <- gsets_hm[[hm_ver]][["gset_name"]]
  row_anno_tab <- gsets_hm[[hm_ver]][["row_anno_tab"]]

  vsd0 <- filterObject(object = vsd,
                       col_filters = cfg$filters)

  rids <- apply(assay(vsd0), 1, stats::var) != 0
  vsd1 <- vsd0[rids, ]
  vsd2 <- vsd1[match(gene_names, rownames(vsd1)), ]

  cdat <- colData(vsd2) %>%
    as.data.frame() %>%
    mutate(across(where(is.factor), droplevels))
  mat <- scaleRows(assay(vsd2))
  rownames(mat) <- convertGeneIDs(rownames(mat), gene_anno, "Ensembl", "HGNC")
  
  hm <- plotHeatmap(
    matrix = mat,
    fill_title = "Expression",
    fill_values = colors_expr,
    column_data = cdat,
    column_anno_list = cfg$anno_list,
    column_split_var = cfg$split_var,
    show_column_names = F,
    show_row_names = (length(gene_names) <= 250),
    row_names_gp = gpar(fontface = "bold", fontsize = 6, 
                        col = ifelse(rownames(mat) %in% c("COL4A1", "LAMC2"), "red", "black")),
    row_title = gset_name,
    row_data = row_anno_tab,
    row_split_var = "celltype",
    row_anno_list = cfg$anno_list,
    row_anno_name_side = "bottom",
    title_size = 9,
    color_seed = 721,
    legend_pos = "right",
    heatmap_width = unit(5, "in"),
    heatmap_height = unit(6, "in"),
    img_file = glue(pb_de_heatmap),
    output_pdf = T
  )
  # ro <- row_order(hm)
  # rd <- row_dend(hm)
  # save(ro, rd, file = gsub(".png", "_RowClustering.rdata", glue(pb_de_heatmap)))
}
```


# DE between Kumar11 cell substates (SuppFig6A)

- Pseudobulks are aggregated by `Kumar 11 Cell States` + `scRNA_case_ID`

```{r}
pb_ver <- "CaseID+CellType"
if(!preprocess){
  de_list <- readRDS(glue(pb_de_out_list))
}
```

## Processing steps

#### Configurations {-}
```{r eval=preprocess}
# hm_ctypes <- c(levels_ctypes_kumar_11ep, levels_ctypes_kumar_3nonep)
# contrast_mat <- matrix(-1/13, nrow = 14, ncol = 14)

hm_ctypes <- c(levels_ctypes_kumar_11ep, "Fibroblasts")
contrast_mat <- matrix(-1/11, nrow = 12, ncol = 12)
diag(contrast_mat) <- 1
contrast_mat <- rbind(contrast_mat, 0) # Batch
contrast_list <- as.list(as.data.frame(contrast_mat))
names(contrast_list) <- c(hm_ctypes)

cfg <- list(
  id_vars = c("scRNA_case_ID_short", "Cell_Type"),

  label = "EachVsRest",
  formula = "~ 0 + Cell_Type + Source",
  contrast_list = contrast_list,

  filters = c("Cell_Type %in% hm_ctypes & Cell_Count > 10"),
  split_var = "Cell_Type",
  anno_list = list(
    # columns
    Cell_Type = list(colors = colors_ctypes, name = "Cell Substate"),
    Broad_Cell_Type = list(colors = colors_ctypes, name = "Cell State"),
    CNV_group_short = list(colors = colors_cnvs, name = "Neoplastic Status"), # for cell-level heatmap
    ER = list(colors = colors_ers, name = "ER Status"), # for cell-level heatmap
    ER_filled = list(colors = colors_ers, name = "ER Status"),
    scRNA_case_ID_short = list(colors = colors_cases_short, name = "Patient"),
    Log_Cell_Count = list(name = "log(Cell Count)"),
    # rows:
    celltype = list(colors = colors_ctypes, name = "Cell Substate", show_legend = F),
    neglog10padj = list(name = "-log10(FDR)")
  )
)
saveRDS(cfg, file = glue(pb_configs))
```

#### Create Pseudobulk `DESeq2` object {-}
```{r eval=preprocess}
dds <- createPseudoBulkDDS(
  object = obj,
  gene_anno = gene_anno,
  id_vars = cfg$id_vars
)
dds <- dds[rowSums(counts(dds)) >= 10, ]
vsd <- vst(dds)
# pryr::object_size(dds)
saveRDS(dds, file = glue(pb_de_in_dds))
saveRDS(vsd, file = glue(pb_de_in_vsd))
# dds <- readRDS(glue(pb_de_in_dds))
# vsd <- readRDS(glue(pb_de_in_vsd))
```

#### Run `DESeq2` {-}
```{r eval=preprocess}
de_list <- runDESeq2(
  object = dds,
  formula_str = cfg$formula,
  contrast_list = cfg$contrast_list,
  filters = cfg$filters,
  inter_file = glue(pb_de_out_dds)
)
saveRDS(de_list, file = glue(pb_de_out_list))
```

## Differentially expressed genes {.tabset}
```{r results='asis', fig.width=14, fig.height=7}
n_tops <- 30

for(de_ver in names(de_list)){
  cat("### ", de_ver, " {.tabset -} \n\n")
  cat("#### Table of top", n_tops, "DEGs {-} \n\n")
  de_list[[de_ver]] %>%
    filter(padj < cutoff_qval) %>%
    head(n = n_tops) %>%
    printKable()
  cat("\n\n")
}
```

## Pseudobulk heatmaps {.tabset}
```{r}
for(hm_ver in c("Top20UpEach", "TopBM")){
  cat("###", hm_ver, "{-} \n\n")
  img <- glue(pb_de_heatmap)
  cat("![](", img, ") \n\n")
  tab <- readRDS(glue(pb_de_heatmap_rowtab))
  printKable(tab)
}
```

```{r eval=preprocess}
TopBM <- de_list[hm_ctypes] %>%
  lapply(function(l){
    l %>%
      arrange(pvalue) %>%
      filter(padj < 0.01) %>%
      filter(log2FoldChange > 0) %>%
      filter(Ensembl %in% gsets$GO[["GO:0005604 basement membrane"]]) %>%
      dplyr::select(Ensembl, HGNC, log2FoldChange, stat, pvalue, padj)
  }) %>%
  map_dfr(~ as.data.frame(.x), .id = "celltype") %>%
  mutate(rank = rank(padj),
         neglog10padj = -log10(padj)) %>%
  # arrange(pvalue)
  group_by(Ensembl) %>%
  slice_min(order_by = rank, n = 1) %>%
  ungroup() %>%
  arrange(celltype, rank)
saveRDS(TopBM, file = glue(pb_de_heatmap_rowtab, hm_ver = "TopBM"))

gsets_hm <- list(
  TopBM = list(
    gset_name = NULL, #"Up-Regulated Basement-Membrane DEGs",
    gene_names = TopBM$Ensembl,
    row_anno_tab = TopBM
  )
)

for(hm_ver in names(gsets_hm)){ # hm_ver="TopBM"
  print(hm_ver)

  gene_names <- gsets_hm[[hm_ver]][["gene_names"]]
  gset_name <- gsets_hm[[hm_ver]][["gset_name"]]
  row_anno_tab <- gsets_hm[[hm_ver]][["row_anno_tab"]]

  vsd0 <- filterObject(object = vsd,
                       col_filters = c(cfg$filters))

  rids <- apply(assay(vsd0), 1, stats::var) != 0
  vsd1 <- vsd0[rids, ]
  vsd2 <- vsd1[match(gene_names, rownames(vsd1)), ]

  cdat <- colData(vsd2) %>%
    as.data.frame() %>%
    mutate(across(where(is.factor), droplevels))
  cdat[["split2"]] <- gsub("-", "\n", cdat[[cfg$split_var]]) %>%
    factor(levels = gsub("-", "\n", levels(cdat[[cfg$split_var]])))
  mat <- scaleRows(assay(vsd2))
  rownames(mat) <- convertGeneIDs(rownames(mat), gene_anno, "Ensembl", "HGNC")

  hm <- plotHeatmap(
    matrix = mat,
    fill_title = "Expression",
    fill_values = colors_expr,
    column_data = cdat,
    column_anno_list = cfg$anno_list,
    column_split_var = "split2", #cfg$split_var,
    show_column_names = F,
    show_row_names = (length(gene_names) <= 250),
    row_names_gp = gpar(fontface = "bold", fontsize = 6, 
                        col = ifelse(rownames(mat) %in% c("COL4A1", "LAMC2"), "red", "black")),
    row_title = gset_name,
    row_data = row_anno_tab,
    row_split_var = "celltype",
    row_anno_list = cfg$anno_list,
    row_anno_name_side = "bottom",
    title_size = 9,
    color_seed = 721,
    legend_pos = "right",
    heatmap_width = unit(10, "in"),
    heatmap_height = unit(7, "in"),
    img_file = glue(pb_de_heatmap),
    output_pdf = T
  )
}
```


# Session Information
```{r echo=F, results='markup'}
sessionInfo()
print(paste("Start Time", stdt))
print(paste("End Time  ", date()))
```

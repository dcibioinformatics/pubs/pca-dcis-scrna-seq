---
title: "QC - PCA scRNASeq"
output: 
  html_document:
    toc: true
    toc_depth: 3
    toc_float:
      collapsed: false
    code_folding: hide
    number_sections: true
editor_options: 
  chunk_output_type: console
---
```{css, echo=F}
/* turn level 4/5 headings into bulleted lists */
/* padding: t r b l */
h4:before {
  content: '\2022';
  padding: 0px 10px 0px 0px;
}
h4 {
  font-size: 15px;
  padding: 0px 0px 0px 10px;
}
h5:before {
  content: '\25CB';
  padding: 0px 10px 0px 0px;
}
h5 {
  font-size: 14px;
  padding: 0px 0px 0px 20px;
}
table {
  white-space: nowrap;
}
```

```{r, include=F}
# preload
htmltools::tagList(DT::datatable(matrix(), extensions = c("Buttons", 'FixedColumns', 'FixedHeader')))
```

Goal: Use the raw Seurat object `Proc/Seurat/BPCA/BPCA_Raw_SeuratObj.rds` to QC cells based on the number of reads (`nCount`), number of features (`nFeature`), and mitochondrial gene percentage (MT%). Save QC-filtered data as `Proc/Seurat/BPCA/BPCA_Filtered_SeuratObj.rds`. 


# Prerequisites

#### Container {-}
```{bash eval=F}
wd=/mnt/data1/workspace/HTAN/scRNASeq
sif=/mnt/data1/public/singularity/dcibioinformaticsR-v2.1.sif
singularity exec --no-home -B ${wd}:${wd}:rw ${sif} R
```

#### Packages and paths {-}
```{r echo=T, message=F, warning=F, results='hide'}
sc_ver <- "BPCA"
flt_ver <- "ByQC"
wd <- "/mnt/data1/workspace/HTAN/scRNASeq"
sapply(list.files(file.path(wd, "Code/Setup"), full.names = T), source)

meta_raw <- readRDS(glue(seurat_raw_meta))

preprocess <- F
if(!preprocess){
  meta_flt <- readRDS(glue(seurat_flt_meta))
}
```

#### QC plot configurations {-}
```{r}
sample_id_var <- "scRNA_sample_ID_short"
sample_id_label <- "Sample ID"
sample_color_var <- "scRNA_sample_type"
sample_color_values <- colors_stypes

group_id_var <- "scRNA_case_ID_short"
group_color_var <- "scRNA_case_ID_short"
group_color_values <- colors_cases_short

ncount_var <- "nCount_RNA"
nfeature_var <- "nFeature_RNA"
mt_var <- "percent.mt"

standard_plot_width <- 14
standard_plot_height <- 7

facet_plot_ncol <- 5
facet_plot_width <- 14
facet_plot_height <- 12

th_list <- list( # c(cl, ch, fl)
  "01D" = c(6, 11.5, 5.6),
  "01N" = c(6, 11.9, 5.6), 
  "02D" = c(6, 12, 5.6), 
  "03N" = c(6, 11.7, 5.6), 
  "04N" = c(6, 11, 5.6),
  "05D" = c(6, 11.9, 5.6),
  "06D" = c(6, 10.6, 5.7),
  "06N" = c(6, 11, 5.6), 
  "07D1" = c(6, 11, 5),
  "07D2" = c(6, 11, 5.7),
  "08D" = c(6.9, 11, 6.1),
  "08N" = c(6, 11.1, 5.6),
  "09D" = c(6, 10.5, 5.4), 
  "09I" = c(6, 10.5, 5), 
  "09N" = c(6.5, 10.5, 5.8),
  "10D1" = c(6, 10.3, 5.2), 
  "10D2" = c(6, 10.4, 5.3),
  "10N" = c(6, 10, 5.4), 
  "11D1" = c(6, 9.6, 5),
  "11D2" = c(6, 10, 5.5),
  "11N1" = c(6, 10.5, 5.5),
  "11N2" = c(6, 9.5, 5),
  "12D" = c(6.2, 10.5, 5.6),
  "12N" = c(6.8, 10, 5.85),
  "13D" = c(6.1, 10.5, 5), 
  "13N" = c(6.1, 10, 5),
  "14I" = c(6, 11.5, 5.6),
  "15D" = c(6, 10.3, 5.5), 
  "15N" = c(6, 11, 5.6), 
  "16D" = c(6, 11.5, 5.7)
) 
```


# QC

## Pre-filtering violin plots {.tabset}

### `nCount` {-}
```{r fig.width=standard_plot_width, fig.height=standard_plot_height, out.height=NULL}
pre_ct_vln <- plotGG(
  data = meta_raw,
  template = "Violin",
  plot_aes_list = list(x = sample_id_var, y = ncount_var, color = sample_color_var),
  main_title = "Pre-filtering",
  x_title = sample_id_label,
  y_title = "Number of molecules detected per cell",
  color_values = sample_color_values,
  facet_formula = paste("~", group_id_var),
  facet_scales = "free_x",
  facet_space = "free_x"
)
print(pre_ct_vln)
```


### `nFeature` {-}
```{r fig.width=standard_plot_width, fig.height=standard_plot_height, out.height=NULL}
pre_ft_vln <- plotGG(
  data = meta_raw,
  template = "Violin",
  plot_aes_list = list(x = sample_id_var, y = nfeature_var, color = sample_color_var),
  main_title = "Pre-filtering",
  x_title = sample_id_label,
  y_title = "Number of genes detected per cell",
  color_values = sample_color_values,
  facet_formula = paste("~", group_id_var),
  facet_scales = "free_x",
  facet_space = "free_x"
)
print(pre_ft_vln)
```

### MT-genes\% {-}
```{r fig.width=standard_plot_width, fig.height=standard_plot_height, out.height=NULL}
pre_mt_vln <- plotGG(
  data = meta_raw,
  template = "Violin",
  plot_aes_list = list(x = sample_id_var, y = mt_var, color = sample_color_var),
  main_title = "Pre-filtering",
  x_title = sample_id_label,
  y_title = "Percentage of mitochondrial genes (MT%) per cell",
  y_limits = c(0, 100),
  color_values = sample_color_values,
  facet_formula = paste("~", group_id_var),
  facet_scales = "free_x",
  facet_space = "free_x"
)
print(pre_mt_vln)
```

### `nCount` vs. `nFeature`, colored by MT\% {-}
```{r fig.width=facet_plot_width, fig.height=facet_plot_height}
pre_scatter <- plotGG(
  data = meta_raw,
  template = "Scatter",
  plot_aes_list = list(x = ncount_var, y = nfeature_var, color = mt_var),
  main_title = "Pre-filtering",
  x_title = "Number of molecules detected per cell",
  y_title = "Number of genes per cell",
	color_title = "MT%",
  color_scheme = "one-sided",
  color_limits = c(0, 100),
  facet_formula = paste("~", sample_id_var),
  facet_fun = "facet_wrap",
  facet_ncol = facet_plot_ncol
)
print(pre_scatter)
```


## Finding thresholds for filtering cells {.tabset}

### By high/low log(`nCount`) {-}
```{r fig.width=facet_plot_width, fig.height=facet_plot_height}
th_tab <- th_list %>%
  map_dfr(
    ~ data.frame(cl = exp(.x[1]), ch = exp(.x[2]), fl = exp(.x[3]), ml = 20),
    .id = sample_id_var
  )
thresdat <- meta_raw %>%
  dplyr::select(all_of(c(sample_id_var, group_id_var))) %>%
  distinct() %>%
  arrange(across(all_of(sample_id_var))) %>%
  left_join(th_tab, by = sample_id_var)

pre_ct_dist <- plotGG(
  data = meta_raw,
  template = "Density",
  plot_aes_list = list(x = ncount_var, color = group_color_var),
  hist_bins = 100,
  main_title = "Pre-filtering",
  x_trans = "log",
  x_title = "Number of molecules detected per cell",
  color_values = group_color_values,
  vline_data = thresdat,
  vline_vars = c("cl", "ch"),
  facet_formula = paste("~", sample_id_var),
  facet_fun = "facet_wrap",
  facet_scales = "free_y",
  facet_ncol = facet_plot_ncol
)
print(pre_ct_dist)
```


### By low log(`nFeature`) {-}
```{r fig.width=facet_plot_width, fig.height=facet_plot_height}
pre_ft_dist <- plotGG(
  data = meta_raw,
  template = "Density",
  plot_aes_list = list(x = nfeature_var, color = group_color_var),
  hist_bins = 100,
  main_title = "Pre-filtering",
  x_trans = "log",
  x_title = "Number of genes detected per cell",
  color_values = group_color_values,
  vline_data = thresdat,
  vline_vars = "fl",
  facet_formula = paste("~", sample_id_var),
  facet_fun = "facet_wrap",
  facet_scales = "free_y",
  facet_ncol = facet_plot_ncol
)
print(pre_ft_dist)
```

### By high MT\% {-}
```{r fig.width=facet_plot_width, fig.height=facet_plot_height}
pre_mt_dist <- plotGG(
  data = meta_raw,
  template = "Density",
  plot_aes_list = list(x = mt_var, color = group_color_var),
  hist_bins = 100,
  main_title = "Pre-filtering",
  x_trans = "identity",
  x_title = "MT%",
  color_values = group_color_values,
  vline_data = thresdat,
  vline_vars = "ml",
  facet_formula = paste("~", sample_id_var),
  facet_fun = "facet_wrap",
  facet_scales = "free_y",
  facet_ncol = facet_plot_ncol
)
print(pre_mt_dist)
```


## Cell counts pre-/post-filtering
```{r eval=preprocess}
obj_raw <- readRDS(glue(seurat_raw_obj))

# filtering
ids <- meta_raw %>%
  mutate(id = colnames(obj_raw)) %>%
  left_join(thresdat) %>%
  dplyr:: filter(
    .data[[ncount_var]] > cl &
    .data[[ncount_var]] < ch &
    .data[[nfeature_var]] > fl &
    .data[[mt_var]] < ml
  ) %>%
  pull(id)

# post-filtering
obj_flt <- obj_raw[, ids]
meta_flt <- obj_flt@meta.data
saveRDS(meta_flt, file = glue(seurat_flt_meta))
saveRDS(obj_flt, file = glue(seurat_flt_obj))
```

```{r}
tab <- compareCellCounts(
  data_pre = meta_raw,
  data_post = meta_flt,
  key_var = sample_id_var
)

printKable(tab)
```


## Post-filtering distribution {.tabset}

### `nCount` {-}
```{r fig.width=facet_plot_width, fig.height=facet_plot_height}
post_ct_dist <- plotGG(
  data = meta_flt,
  template = "Density",
  plot_aes_list = list(x = ncount_var, color = group_color_var),
  hist_bins = 100,
  main_title = "Post-filtering",
  x_trans = "log",
  x_title = "Number of molecules detected per cell",
  color_values = group_color_values,
  vline_data = thresdat,
  vline_vars = c("cl", "ch"),
  facet_formula = paste("~", sample_id_var),
  facet_fun = "facet_wrap",
  facet_scales = "free_y",
  facet_ncol = facet_plot_ncol
)
print(post_ct_dist)
```

### `nFeature` {-}
```{r fig.width=facet_plot_width, fig.height=facet_plot_height}
post_ft_dist <- plotGG(
  data = meta_flt,
  template = "Density",
  plot_aes_list = list(x = nfeature_var, color = group_color_var),
  hist_bins = 100,
  main_title = "Post-filtering",
  x_trans = "log",
  x_title = "Number of genes detected per cell",
  color_values = group_color_values,
  vline_data = thresdat,
  vline_vars = "fl",
  facet_formula = paste("~", sample_id_var),
  facet_fun = "facet_wrap",
  facet_scales = "free_y",
  facet_ncol = facet_plot_ncol
)
print(post_ft_dist)
```

### MT\% {-}
```{r fig.width=facet_plot_width, fig.height=facet_plot_height}
post_mt_dist <- plotGG(
  data = meta_flt,
  template = "Density",
  plot_aes_list = list(x = mt_var, color = group_color_var),
  hist_bins = 100,
  main_title = "Post-filtering",
  x_trans = "identity",
  x_title = "MT%",
  color_values = group_color_values,
  vline_data = thresdat,
  vline_vars = "ml",
  facet_formula = paste("~", sample_id_var),
  facet_fun = "facet_wrap",
  facet_scales = "free_y",
  facet_ncol = facet_plot_ncol
)
print(post_mt_dist)
```


## Post-filtering violin plots {.tabset}

### `nCount` {-}
```{r fig.width=standard_plot_width, fig.height=standard_plot_height, out.height=NULL}
post_ct_vln <- plotGG(
  data = meta_flt,
  template = "Violin",
  plot_aes_list = list(x = sample_id_var, y = ncount_var, color = sample_color_var),
  main_title = "Post-filtering",
  x_title = sample_id_label,
  y_title = "Number of molecules detected per cell",
  color_values = sample_color_values,
  facet_formula = paste("~", group_id_var),
  facet_scales = "free_x",
  facet_space = "free_x"
)
print(post_ct_vln)
```


### `nFeature` {-}
```{r fig.width=standard_plot_width, fig.height=standard_plot_height, out.height=NULL}
post_ft_vln <- plotGG(
  data = meta_flt,
  template = "Violin",
  plot_aes_list = list(x = sample_id_var, y = nfeature_var, color = sample_color_var),
  main_title = "Post-filtering",
  x_title = sample_id_label,
  y_title = "Number of genes detected per cell",
  color_values = sample_color_values,
  facet_formula = paste("~", group_id_var),
  facet_scales = "free_x",
  facet_space = "free_x"
)
print(post_ft_vln)
```

### MT-genes\% {-}
```{r fig.width=standard_plot_width, fig.height=standard_plot_height, out.height=NULL}
post_mt_vln <- plotGG(
  data = meta_flt,
  template = "Violin",
  plot_aes_list = list(x = sample_id_var, y = mt_var, color = sample_color_var),
  main_title = "Post-filtering",
  x_title = sample_id_label,
  y_title = "Percentage of mitochondrial genes (MT%) per cell",
  y_limits = c(0, 100),
  color_values = sample_color_values,
  facet_formula = paste("~", group_id_var),
  facet_scales = "free_x",
  facet_space = "free_x"
)
print(post_mt_vln)
```

### `nCount` vs. `nFeature`, colored by MT\% {-}
```{r fig.width=facet_plot_width, fig.height=facet_plot_height}
post_scatter <- plotGG(
  data = meta_flt,
  template = "Scatter",
  plot_aes_list = list(x = ncount_var, y = nfeature_var, color = mt_var),
  main_title = "Post-filtering",
  x_title = "Number of molecules detected per cell",
  y_title = "Number of genes per cell",
	color_title = "MT%",
  color_scheme = "one-sided",
  color_limits = c(0, 100),
  facet_formula = paste("~", sample_id_var),
  facet_fun = "facet_wrap",
  facet_ncol = facet_plot_ncol
)
print(post_scatter)
```

## Export (SuppFig1A-E)
```{r}
# pdf.options(useDingbats = TRUE) # https://github.com/yihui/knitr/issues/311
# run the following manually, `useDingbats = TRUE` is not recognized by rmd for some reasons.
# ggsave(filename = img, plot = p, dev = "pdf", useDingbats = T, paper = "special",
#        width = standard_plot_width, height = standard_plot_height * 2, units = "in", dpi = 300)

img <- glue(seurat_flt_qc, qc_ver = "1_nCount_Violin")
p <- pre_ct_vln / post_ct_vln
p <- applyConsistentYlims(p)
ggsave(filename = img, plot = p, dpi = 300, units = "in", 
       width = standard_plot_width, height = standard_plot_height * 2)

img <- glue(seurat_flt_qc, qc_ver = "2_nFeature_Violin")
p <- pre_ft_vln / post_ft_vln
p <- applyConsistentYlims(p)
ggsave(filename = img, plot = p, dpi = 300, units = "in", 
       width = standard_plot_width, height = standard_plot_height * 2)

img <- glue(seurat_flt_qc, qc_ver = "3_MT_Violin")
p <- pre_mt_vln / post_mt_vln
p <- applyConsistentYlims(p)
ggsave(filename = img, plot = p, dpi = 300, units = "in", 
       width = standard_plot_width, height = standard_plot_height * 2)

img <- glue(seurat_flt_qc, qc_ver = "4_Scatter")
p <- pre_scatter / post_scatter
p <- applyConsistentYlims(p)
ggsave(filename = img, plot = p, dpi = 300, units = "in", 
       width = standard_plot_width, height = standard_plot_height * 2)

img <- glue(seurat_flt_qc, qc_ver = "5_CellCountsTable")
cp_grob <- tab %>%
  setNames(nm = c(sample_id_label, "Pre-filtering", "Post-filtering", "Cells Kept%")) %>%
  tableGrob()
ggsave(filename = img, plot = cp_grob, dpi = 300, units = "in", width = 7, height = 10)
```


# Session Information
```{r echo=F, results='markup'}
sessionInfo()
print(paste("Start Time", stdt))
print(paste("End Time  ", date()))
```
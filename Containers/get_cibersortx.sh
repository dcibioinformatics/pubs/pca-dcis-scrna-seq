# see: https://hub.docker.com/r/cibersortx/fractions/tags

docker -v # Docker version 24.0.6, build ed223bc
docker pull cibersortx/fractions
docker image ls cibersortx/fractions
docker run cibersortx/fractions

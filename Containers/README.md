---
title: "System Requirements and Software Dependencies"
output: 
  html_document:
    keep_md: yes
    toc: false
    number_sections: true
---




## Preprocessing: Base Pipeline

- Operating system

```
## Ubuntu 20.04.6 LTS \n \l
```

- `Python` version

```
## Python 3.8.10
```

- `Python` modules

```
## INFO: Successfully output requirements
```


## Preprocessing: `Cellranger` 

- `Singularity` version

```
## 1.3.5
```

- Operating system of the container image

```
## Debian GNU/Linux 10 \n \l
```

- `Cellranger` version

```
## cellranger cellranger-6.0.1
```


## `CIBERSORTx` Analysis

- `Docker` version

```
## Docker version 27.4.1, build b9d17ea
```

- `CIBERSORTx` image

```
## REPOSITORY             TAG       IMAGE ID       CREATED       SIZE
## cibersortx/fractions   latest    82a17fe6bf93   4 years ago   966MB
```


## Other `R`-based Analyses: 

- `Singularity` version

```
## 1.3.5
```

- Operating system of the container image

```
## Ubuntu 20.04.6 LTS \n \l
```

- `R` version

```
## R version 4.2.2 (2022-10-31) -- "Innocent and Trusting"
## Copyright (C) 2022 The R Foundation for Statistical Computing
## Platform: x86_64-pc-linux-gnu (64-bit)
## 
## R is free software and comes with ABSOLUTELY NO WARRANTY.
## You are welcome to redistribute it under the terms of the
## GNU General Public License versions 2 or 3.
## For more information about these matters see
## https://www.gnu.org/licenses/.
```

- Key `R` packages in alphabetical order

```
##           Package Version
## 1  ComplexHeatmap  2.15.4
## 2          DESeq2  1.38.3
## 3              DT    0.28
## 4   GenomicRanges  1.50.2
## 5         QDNAseq  1.34.0
## 6          Seurat 4.3.0.1
## 7             ape   5.7-1
## 8           aplot  0.1.10
## 9         biomaRt  2.54.1
## 10       circlize  0.4.15
## 11     doParallel  1.0.17
## 12          fgsea  1.24.0
## 13        foreach   1.5.2
## 14           gage  2.48.0
## 15          ggh4x   0.2.5
## 16        ggpmisc 0.5.4-1
## 17           ggpp   0.5.4
## 18         ggpubr   0.6.0
## 19        ggrepel   0.9.3
## 20         ggtree   3.6.2
## 21           glue   1.6.2
## 22       infercnv  1.10.1
## 23     kableExtra   1.3.4
## 24          knitr    1.43
## 25        monocle  2.26.0
## 26        msigdbr   7.5.1
## 27       openxlsx 4.2.5.2
## 28      patchwork   1.3.0
## 29        rstatix   0.7.2
## 30       scSorter   0.0.2
## 31         scales   1.3.0
## 32         table1   1.4.3
## 33      tidyverse   2.0.0
## 34  treedataverse   0.0.1
## 35        viridis   0.6.4
```

- Python envronment for `InferCNV` in the container image

```
## Python 3.8.10
```


